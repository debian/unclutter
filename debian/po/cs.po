# Czech translation of unclutter debconf messages.
# 2005 Miroslav Kure <kurem@debian.cz>
# This file is distributed under the same license as the unclutter package.
#
msgid ""
msgstr ""
"Project-Id-Version: unclutter\n"
"Report-Msgid-Bugs-To: unclutter@packages.debian.org\n"
"POT-Creation-Date: 2008-07-14 06:17+0000\n"
"PO-Revision-Date: 2005-02-11 13:15+0100\n"
"Last-Translator: Miroslav Kure <kurem@debian.cz>\n"
"Language-Team: Czech <debian-l10n-czech@debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../unclutter.templates:1001
msgid "Start unclutter automatically?"
msgstr "Spustit unclutter automaticky?"

#. Type: boolean
#. Description
#: ../unclutter.templates:1001
msgid ""
"unclutter can be started automatically after X has been started for a user. "
"That can be either after startx from console or after login with a display "
"manager."
msgstr ""
"unclutter se m��e spustit automaticky po p�ihl�en� u�ivatele do X Window "
"Systemu (a� u� p��kazem startx nebo p�ihl�en�m p�es grafick�ho spr�vce "
"obrazovky)."
